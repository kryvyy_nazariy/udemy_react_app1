var React = require('react');
var ReactDOM = require('react-dom');
var Greeter = require('Greeter')

var firstName = 'Jen';
var inputMessage = 'New message'

ReactDOM.render(
  <Greeter name={firstName} message={inputMessage} />,

  document.getElementById("app")
);